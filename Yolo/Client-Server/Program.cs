﻿using System;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace SocketClient
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                SendMessageFromSocket(11000, true);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                Console.ReadLine();
            }
        }

        static void SendMessageFromSocket(int port, bool recvFlag)
        {
            byte[] bytes = new byte[1024];

            IPHostEntry ipHost = Dns.GetHostEntry("localhost");
            IPAddress ipAddr = ipHost.AddressList[0];
            IPEndPoint ipEndPoint = new IPEndPoint(ipAddr, port);

            Socket sender = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

            sender.Connect(ipEndPoint);

            if (recvFlag){
                int byt = sender.Receive(bytes);

                Console.WriteLine("{0}\n", Encoding.UTF8.GetString(bytes, 0, byt));
            }

            string message = Console.ReadLine();

            byte[] msg = Encoding.UTF8.GetBytes(message);

            int bytesSent = sender.Send(msg);

            int bytesRec = sender.Receive(bytes);

            Console.WriteLine("{0}\n", Encoding.UTF8.GetString(bytes, 0, bytesRec));

            if (Encoding.UTF8.GetString(bytes, 0, bytesRec).IndexOf("You lose") == -1)
                SendMessageFromSocket(port, false);

            sender.Shutdown(SocketShutdown.Both);
            sender.Close();
        }
    }
}