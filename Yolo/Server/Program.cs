using System;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace SocketServer
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] dictionary = { "thunder", "bucket", "building", "credit", "profit", "calendar", "treatment", "substance", "tongue", "system", "afternoon",
                                    "chance", "governor", "pleasure", "island", "activity", "insurance", "mountain", "chicken", "appliance", "question", "surprise",
                                    "invention", "development", "condition", "observation", "agreement", "cushion", "giraffe", "plantation", "regret", "needle",
                                    "afterthought", "grandfather", "toothbrush", "competition", "decision", "account", "boundary", "pancake", "window", "umbrella",
                                    "advertisement", "government", "basketball", "scarecrow", "copper", "laborer", "arithmetic", "donkey", "discussion" };

            Random rand = new Random();
            int randomWordIndex = rand.Next(dictionary.Length);
            bool firstMessageFlag = false, playerWinFlag = false, playerLoseFlag = false;
            int wrongAnswerCounter = 0;
            StringBuilder word = null;

            IPHostEntry ipHost = Dns.GetHostEntry("localhost");
            IPAddress ipAddr = ipHost.AddressList[0];
            IPEndPoint ipEndPoint = new IPEndPoint(ipAddr, 11000);

            Socket sListener = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

            try
            {
                sListener.Bind(ipEndPoint);
                sListener.Listen(10);

                while (true) { 

                    if (playerLoseFlag){
                        break;
                    }

                    Socket handler = sListener.Accept();
                    string data = null;
                    string reply = null;
                    byte[] message;

                    if (!firstMessageFlag)
                    {
                        word = new StringBuilder(dictionary[randomWordIndex][0].ToString());

                        for (int i = 0; i < dictionary[randomWordIndex].Length - 1; ++i)
                        {
                            word.Append('_');
                        }

                        message = Encoding.UTF8.GetBytes(word.ToString() + "\n");

                        handler.Send(message);

                        firstMessageFlag = true;
                    }

                    byte[] bytes = new byte[1024];
                    int bytesRec = handler.Receive(bytes);

                    data += Encoding.UTF8.GetString(bytes, 0, bytesRec);

                    Console.Write("Recieved: " + data + "\n");

                    if (dictionary[randomWordIndex].Equals(data)) {

                        Console.WriteLine("Player won.\n");

                        reply = "You won\n";

                        message = Encoding.UTF8.GetBytes(reply);

                        handler.Send(message);
                        break;
                    } else if ((dictionary[randomWordIndex].Contains(data)) && (data.Length == 1)) {

                        playerWinFlag = true;

                        for(int i = 0; i < word.Length; ++i){
                            if (dictionary[randomWordIndex][i] == data[0]){
                                word[i] = data[0];
                            }
                            if(word[i] == '_'){
                                playerWinFlag = false;
                            }
                        }

                        reply = word.ToString() + "\n" + (playerWinFlag ? "You won\n" : "");

                        message = Encoding.UTF8.GetBytes(reply);

                        handler.Send(message);

                        if (playerWinFlag){
                            break;
                        }
                    } else if ((!dictionary[randomWordIndex].Contains(data)) && (data.Length == 1)){

                        Console.WriteLine("Player gave wrong answer\n");

                        string gibbet = null;

                        switch (wrongAnswerCounter){

                            case 0:
                                gibbet = "o";
                                break;
                            case 1:
                                gibbet = "o>";
                                break;
                            case 2:
                                gibbet = "o>-";
                                break;
                            case 3:
                                gibbet = "o>-<";
                                break;
                        }

                        reply = "Wrong answer\n" + gibbet + "\n" + (wrongAnswerCounter == 3? "You lose\nThe right answer is - " + dictionary[randomWordIndex] : "");

                        message = Encoding.UTF8.GetBytes(reply);

                        handler.Send(message);

                        ++wrongAnswerCounter;

                        if(wrongAnswerCounter > 3){
                            Console.WriteLine("Player lost\n");
                            break;
                        }
                    }

                    handler.Shutdown(SocketShutdown.Both);
                    handler.Close();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                Console.ReadLine();
            }
        }
    }
}